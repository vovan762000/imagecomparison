package utils;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

public class Util {

    private static final double MAX_DIFFERENCE_PERCENT = 10;

    public static int[][] createMatrixDifferences(BufferedImage image1, BufferedImage image2) {
        int[][] matrix = new int[image1.getWidth()][image1.getHeight()];
        for (int y = 0; y < image1.getHeight(); y++) {
            for (int x = 0; x < image1.getWidth(); x++) {
                matrix[x][y] = isDifferent(x, y, image1, image2) ? 1 : 0;
            }
        }
        return matrix;
    }

    public static boolean isDifferent(int x, int y, BufferedImage image1, BufferedImage image2) {
        int difference = 0;
        int rgbA = image1.getRGB(x, y);
        int rgbB = image2.getRGB(x, y);
        int red1 = (rgbA >> 16) & 0xff;
        int green1 = (rgbA >> 8) & 0xff;
        int blue1 = (rgbA) & 0xff;
        int red2 = (rgbB >> 16) & 0xff;
        int green2 = (rgbB >> 8) & 0xff;
        int blue2 = (rgbB) & 0xff;
        difference += Math.abs(red1 - red2);
        difference += Math.abs(green1 - green2);
        difference += Math.abs(blue1 - blue2);

        double percentage = (difference
                / 255) * 100;
        return percentage > MAX_DIFFERENCE_PERCENT ? true : false;
    }

    public static BufferedImage makeCopy(BufferedImage image) {
        ColorModel cm = image.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = image.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }


}
