package utils;

public class RectanglePoints {

    private final int MAX_DISTANCE = 20;
    private int xLeft;
    private int xRight;
    private int yDown;
    private int yUp;

    public RectanglePoints(int yUp) {
        this.yUp = yUp;
    }

    public void calculateTmpPoints(int xL, int xR, int yU, int[][] matrix) {

        xLeft = xL - MAX_DISTANCE;
        xRight = xR + MAX_DISTANCE;
        yDown = yU + MAX_DISTANCE;

        if (xLeft < 0) {
            xLeft = 0;
        }
        if (xRight > matrix.length - 1) {
            xRight = matrix.length - 1;
        }
        if (yDown > matrix[xL].length - 1) {
            yDown = matrix[xL].length - 1;
        }
        for (int i = yUp; i <= yDown; i++) {
            if (matrix[xLeft][i] == 1 && xLeft != 0) {
                calculateTmpPoints(xLeft, xR, yU, matrix);
            }
            if (matrix[xRight][i] == 1 && xRight != matrix.length - 1) {
                calculateTmpPoints(xL, xRight, yU, matrix);
            }
        }
        for (int i = xLeft; i <= xRight; i++) {
            if (matrix[i][yDown] == 1 && yDown != matrix[xL].length - 1) {
                calculateTmpPoints(xL, xR, yDown, matrix);
            }
        }
    }

    void findXLeft(int xL, int xR, int yU, int yD, int[][] matrix) {
        for (int j = yU; j <= yD; j++) {
            if (matrix[xL][j] == 1) {
                xLeft = xL;
                break;
            }
            if (j == yD) {
                findXLeft(++xL, xR, yU, yD, matrix);
            }
        }
    }

    void findXRight(int xL, int xR, int yU, int yD, int[][] matrix) {
        for (int j = yU; j <= yD; j++) {
            if (matrix[xR][j] == 1) {
                xRight = xR;
                break;
            }
            if (j == yD) {
                findXRight(xL, --xR, yU, yD, matrix);
            }
        }
    }

    void findYDown(int xL, int xR, int yU, int yD, int[][] matrix) {
        for (int j = xL; j <= xR; j++) {
            if (matrix[j][yD] == 1) {
                yDown = yD;
                break;
            }
            if (j == xR) {
                findYDown(xL, xR, yU, --yD, matrix);
                yD--;
            }
        }
    }

    public void finalizePoints(int xL, int xR, int yU, int yD, int[][] matrix) {
        findXLeft(xL, xR, yU, yD, matrix);
        findXRight(xL, xR, yU, yD, matrix);
        findYDown(xL, xR, yU, yD, matrix);
        for (int i = xLeft; i <= xRight; i++) {
            for (int j = yUp; j <= yDown; j++) {
                matrix[i][j] = 0;
            }
        }
    }

    public int getxLeft() {
        return xLeft;
    }

    public int getxRight() {
        return xRight;
    }

    public int getyDown() {
        return yDown;
    }

    public int getyUp() {
        return yUp;
    }

}
