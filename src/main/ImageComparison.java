package main;

import static java.awt.Color.RED;
import static utils.Util.*;
import static validators.Validator.*;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import utils.RectanglePoints;

public class ImageComparison {
    
    private final int PIC_NUMBER = 2;
    private int[][] matrix;
    private String[] masPath = new String[PIC_NUMBER];
    
    private JFrame frame;
    private JButton button = new JButton("Input");
    private JTextField input = new JTextField("", 23);
    private JLabel label = new JLabel("Input path first file:");
    private JLabel picLabel = new JLabel();
    private Container container;
    
    public ImageComparison() {
        frame = new JFrame("Image Comparison");
        frame.setBounds(10, 10, 1000, 900);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        container = frame.getContentPane();
        container.setLayout(new FlowLayout());
        container.add(label);
        container.add(input);
        button.addActionListener(new ImageComparison.ButtonEventListener());
        container.add(button);
        frame.setVisible(true);
    }
    
    public static void main(String[] args) throws IOException {
        
        new ImageComparison();
    }
    
    private class ButtonEventListener implements ActionListener {
        
        private int elMas = 0;
        
        public void actionPerformed(ActionEvent e) {
            if (isPicture(input.getText()) && masPath[masPath.length - 1] == null) {
                masPath[elMas] = input.getText();
                input.setText("");
                label.setText("Input path second file:");
                elMas++;
            } else {
                input.setText("Ivalid file");
                label.setText("Input path first file:");
                elMas = 0;
            }
            
            if (masPath[masPath.length - 1] != null) {
                BufferedImage img1 = null;
                BufferedImage img2 = null;
                try {
                    img1 = ImageIO.read(new File(Paths.get(masPath[0]).toString().trim()));
                    img2 = ImageIO.read(new File(Paths.get(masPath[1]).toString()));
                } catch (IOException ex) {
                    ex.getStackTrace();
                }
                if (isCorrectSize(img1, img2)) {
                    matrix = createMatrixDifferences(img1, img2);
                    BufferedImage outImg = makeCopy(img2);
                    Graphics2D graphics = outImg.createGraphics();
                    graphics.setColor(RED);
                    
                    for (int j = 0; j < matrix[0].length; j++) {
                        for (int i = 0; i < matrix.length; i++) {
                            if (matrix[i][j] == 1) {
                                RectanglePoints rp = new RectanglePoints(j);
                                rp.calculateTmpPoints(i, i, j, matrix);
                                rp.finalizePoints(rp.getxLeft(), rp.getxRight(), rp.getyUp(), rp.getyDown(), matrix);
                                graphics.drawRect(rp.getxLeft(), rp.getyUp(),
                                        rp.getxRight() - rp.getxLeft(), rp.getyDown() - rp.getyUp());
                            }
                        }
                    }
                    container.remove(button);
                    container.remove(input);
                    label.setText("Result image");
                    picLabel = new JLabel(new ImageIcon(outImg));
                    container.add(picLabel);
                    frame.setVisible(true);
                }
            }
            
        }
    }
    
}
