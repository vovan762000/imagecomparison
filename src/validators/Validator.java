package validators;

import java.awt.image.BufferedImage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private static final String IMAGE_PATTERN
            = "([^\\s]+(\\.(?i)(jpg|png))$)";

    public static boolean isPicture(String testString) {
        Pattern p = Pattern.compile(IMAGE_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    public static boolean isCorrectSize(BufferedImage image1, BufferedImage image2) {
        if (image1.getHeight() != image2.getHeight() || image1.getWidth() != image2.getWidth()) {
            return false;
        }
        return true;
    }
}
